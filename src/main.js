var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var x1 = 0
var y1 = 0
var frameRatio = [0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015, 0.005 + Math.random() * 0.015]
var lineHistory = []

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth * 0.5, windowHeight * 0.5, boardSize, boardSize)

  for (var i = 0; i < lineHistory.length; i++) {
    noFill()
    stroke(255 * (i / lineHistory.length))
    strokeWeight(boardSize * 0.02)
    push()
    translate(windowWidth * 0.5, windowHeight * 0.5)
    line(lineHistory[i][0], lineHistory[i][1], lineHistory[i][2], lineHistory[i][3])
    pop()
  }

  noFill()
  stroke(255)
  strokeWeight(boardSize * 0.02)
  push()
  translate(windowWidth * 0.5, windowHeight * 0.5)
  line(sin(frameCount * frameRatio[0] * x1 + cos(frameCount * frameRatio[1])) * boardSize * 0.4, cos(frameCount * frameRatio[2] * y1 + sin(frameCount * frameRatio[3])) * boardSize * 0.4, cos(frameCount * frameRatio[4]) * boardSize * 0.4, sin(frameCount * frameRatio[5]) * boardSize * 0.4)
  pop()

  x1 = noise(y1)
  y1 = noise(x1)

  // TODO: fix responsiveness issue

  lineHistory.push([sin(frameCount * frameRatio[0] * x1 + cos(frameCount * frameRatio[1])) * boardSize * 0.4, cos(frameCount * frameRatio[2] * y1 + sin(frameCount * frameRatio[3])) * boardSize * 0.4, cos(frameCount * frameRatio[4]) * boardSize * 0.4, sin(frameCount * frameRatio[5]) * boardSize * 0.4])
  if (lineHistory.length > 420) {
    lineHistory = lineHistory.splice(1)
  }

}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
